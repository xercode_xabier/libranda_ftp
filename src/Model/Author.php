<?php 

namespace App\Model;

class Author extends Contributor
{
    public function __construct()
    {
        parent::setRole('A01');
    }
}