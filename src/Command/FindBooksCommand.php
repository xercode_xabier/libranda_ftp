<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;



class FindBooksCommand extends Command
{
    const COMMAND_NAME = 'find:eBooks-copy';

    /**
     * @var array
     */
    private $isbns;

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Find files by isbn and copy to oput dir');
        $this->addArgument('input-dir', InputArgument::REQUIRED, 'The directory with ebooks.');
        $this->addArgument('output-dir', InputArgument::REQUIRED, 'The directory save ebooks.');
        $this->addOption('match', '-m',InputArgument::OPTIONAL, 'Match with list.', false);

        $this->isbns = $this->readCsvFile();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $inputDir   = $input->getArgument('input-dir');
        $outputDir  = $input->getArgument('output-dir');
        $match      = (boolean) $input->getOption('match');

        if (!file_exists($inputDir) || !is_dir($inputDir)) {
            $output->writeln('<error>'.sprintf('The directory input "%s" not exits or  can not reader', $inputDir).'</error>');
            return;
        }

        if (!file_exists($outputDir) || !is_dir($outputDir)) {
            $output->writeln('<error>'.sprintf('The directory output  "%s" not exits or  can not reader', $outputDir).'</error>');
            return;
        }

        $finder = new Finder();
        $fileSystem = new Filesystem();

        $directories = $finder->directories()->in($inputDir)->depth('< 1');

        foreach($directories->getIterator() as $iterator) {
            $directory = $iterator->getPathname();
            $finder = new Finder();
            $finder->files()->in($directory);
            foreach ($finder as $file) {
                $baseName           = $file->getBasename('.'.$file->getExtension());
                $fileFullPath       = $file->getPathname();
                $fileCopyFullPath   = $outputDir.'/'.$file->getBasename();
                if (in_array($baseName, $this->isbns)) {
                    $fileSystem->copy($fileFullPath, $fileCopyFullPath, true);
                    $message = '<info>'.sprintf('Copy file %s to %s', $file->getBasename(), $outputDir).'</info>';
                    $output->writeln($message);
                }
            }
        }


    }


    protected  function readCsvFile()
    {
        $csvEncoder = new CsvEncoder();
        $content    = file_get_contents(realpath(__DIR__.'/librospendientes.csv'));
        $fileData = $csvEncoder->decode($content, 'csv');
        $isbns = [];

        foreach ($fileData as $item) {
            $isbns[] =  $item['isbn'];
        }

        return $isbns;
    }


}