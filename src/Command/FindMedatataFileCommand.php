<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;



class FindMedatataFileCommand extends Command
{
    const COMMAND_NAME = 'find:eBooks-metadata';

    /**
     * @var array
     */
    private $isbns;

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Find files ');
        $this->addArgument('input-dir', InputArgument::REQUIRED, 'The directory with metadata.');
        $this->addArgument('output-dir', InputArgument::REQUIRED, 'The directory save metadata.');
        $this->addOption('match', '-m',InputArgument::OPTIONAL, 'Match with list.', false);

        $this->isbns = $this->readCsvFile();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $inputDir   = $input->getArgument('input-dir');
        $outputDir  = $input->getArgument('output-dir');
        $match      = (boolean) $input->getOption('match');

        if (!file_exists($inputDir) || !is_dir($inputDir)) {
            $output->writeln('<error>'.sprintf('The directory input "%s" not exits or  can not reader', $inputDir).'</error>');
            return;
        }

        if (!file_exists($outputDir) || !is_dir($outputDir)) {
            $output->writeln('<error>'.sprintf('The directory output  "%s" not exits or  can not reader', $outputDir).'</error>');
            return;
        }

        $finder = new Finder();
        $fileSystem = new Filesystem();

        $directories = $finder->directories()->in($inputDir)->depth('< 1');

        foreach($directories->getIterator() as $iterator) {
            $directory = $iterator->getPathname();
            $finder = new Finder();
            $finder->files()->in($directory);
            foreach ($finder as $file) {
                if ($file->getExtension() === 'onix') {
                    $fileFullPath       = $file->getPathname();
                    $fileCopyFullPath   = $outputDir.'/'.$file->getBasename();
                    $parser = new \App\OnixParser($file->getPathname(), true);
                    foreach($parser->getOnix()->getProducts() as $product) {
                        $isbn = $product->getISBN();

                        if (in_array($isbn, $this->isbns)) {
                            $message = '<info>'.sprintf('Encontrado ISBN: %s , Copy file %s to %s', $isbn, $fileFullPath, $outputDir).'</info>';
                            $fileSystem->copy($fileFullPath, $fileCopyFullPath, true);
                            $output->writeln($message);
                            break ;
                        }
                    }
                }
            }
        }


    }


    public function getIdentifiers(\DOMDocument $record)
    {
        $identifiers = [];

        die(print_r($record->get('Product'), true));
        foreach ($record->Product as $product) {
            $productIdentifier = $product->ProductIdentifier;
            $recordReference = $record->RecordReference;
            if (null !== $productIdentifier) {
                $productIDType = (string) $productIdentifier->ProductIDType;
                switch ($productIDType ) {
                    case '02':
                    case '03':
                    case '06':
                    case '15':
                        $identifiers[] = self::cleanIdentifier((string) $productIdentifier->IDValue);
                }
            } elseif(null !== $recordReference){
                $identifiers[] = self::cleanIdentifier((string) $recordReference);
            } else {
                // null;
            }
        }

        return $identifiers;
    }

    /**
     * @param $string
     * @return string
     */
    public static function cleanIdentifier($string)
    {
        $identifier = trim($string, ' ');
        $identifier = str_replace('-', '', $identifier);

        return (string) $identifier;
    }

    protected  function readCsvFile()
    {
        $csvEncoder = new CsvEncoder();
        $content    = file_get_contents(realpath(__DIR__.'/librospendientes.csv'));
        $fileData = $csvEncoder->decode($content, 'csv');
        $isbns = [];

        foreach ($fileData as $item) {
            $isbns[] =  $item['isbn'];
        }

        return $isbns;
    }


}